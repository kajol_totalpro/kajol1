
$(document).ready(function () {

    Array.prototype.remove = function (v) {
        delete this[this.indexOf(v)]
    };

    $('#add_transport').on('click', function () {
        
        var error = new Array();

        //regular expression
        var mobileExp = /^\d{10}$/;

        var name = $('#list1').val().trim();
        var remarks = $('#list2').val().trim();

        var url = $('#myform_id').attr('action');

        if (name == "") {
            error.push($('#list1').attr('id'));
            $('#list1').next('span').text('Enter Company Name!');
            $('#' + error[0]).focus();
        } else {
            error.remove($('#list1').attr('id'));
            $('#list1').next('span').text("");
        }

        if (remarks == "") {
            error.push($('#list2').attr('id'));
            $('#list2').next('span').text('Enter Remarks!');
            $('#' + error[0]).focus();
        } else {
            error.remove($('#list2').attr('id'));
            $('#list2').next('span').text("");
        }

        if (error.length <= 0) {
            $btn = $(this).val();

            if ($btn == 'Add New')
            {

                var fd = new FormData();

                fd.append('t_name', name);
                fd.append('t_remarks', remarks);

                $.ajax({
                    url: url + 'Admin/Transport/Insert/add_new',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    dataType: 'Json',
                    success: function (data) {
                        console.log(data);
                        if (data.flag == 'error')
                        {
                            $.toast({
                                heading: 'Error',
                                position: 'top-right',
                                loaderBg: '#ffffff',
                                icon: 'warning',
                                hideAfter: 2000,
                                stack: 6
                            });
                            $("#pleasewait").fadeOut();
                        } else if (data.flag == 'success')
                        {
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                loaderBg: '#ffffff',
                                icon: 'success',
                                hideAfter: 2000,
                                stack: 6
                            });
                            window.setTimeout(function () {
                                window.location.href = url + "transport/view";
                            }, 2000);
                        }
                    }
                });
            } else if ($btn == 'Update') {
                var fd = new FormData();

                fd.append('t_name', name);
                fd.append('t_remarks', remarks);

                var uid = $('#transport_id').val().trim();
                fd.append('t_id', uid);

                $.ajax({
                    url: url + 'Admin/Transport/Update/update_data',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    dataType: 'Json',
                    success: function (data) {

                        if (data.flag == 'error')
                        {
                            $.toast({
                                heading: 'Error',
                                position: 'top-right',
                                loaderBg: '#ffffff',
                                icon: 'warning',
                                hideAfter: 2000,
                                stack: 6
                            });
                            $("#pleasewait").fadeOut();
                        } else if (data.flag == 'success')
                        {
                            $.toast({
                                heading: 'Success',
                                position: 'top-right',
                                loaderBg: '#ffffff',
                                icon: 'success',
                                hideAfter: 2000,
                                stack: 6
                            });
                            window.setTimeout(function () {
                                window.location.href = url + "transport/view";
                            }, 2000);
                        }
                    }
                });
            }

        }


    });

     $('.my_transport_btn').on('click', function () {
        
        var conf = confirm('Are you sure want to delete?');
        if (conf)
        {
            var id = $(this).attr('transport_id');
            var url = $(this).attr('url');
            $.ajax({
                type: "POST",
                url: url + 'Admin/Transport/delete/',
                data: {"id": id},
                dataType: "json",
                async: false,
                success: function (data) {
                        $.toast({
                                heading: 'Delete Successfully',
                                position: 'top-right',
                                loaderBg: '#ffffff',
                                icon: 'success',
                                hideAfter: 1000,
                                stack: 6
                            });
                    
                        console.log(data);
                        window.setTimeout(function () {
                            window.location.href = url + "transport/view";
                        }, 1000);
                  
                }
            });

        }
    });

});