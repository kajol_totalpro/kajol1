<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url()?>assets/plugins/images/favicon.png">
<title>Student Registration</title>
<!-- Bootstrap Core CSS -->
<link href="<?= base_url()?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?= base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?= base_url()?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- morris CSS -->
<link href="<?= base_url()?>assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?= base_url()?>assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?= base_url()?>assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?= base_url()?>assets/css/colors/default.css" id="theme" rel="stylesheet">
<link href="<?= base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url()?>assets/cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

<script src="<?= base_url()?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
</head>