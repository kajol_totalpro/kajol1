<style>
    @import "compass/css3";

    * { box-sizing: border-box; }

    body {
        font-family: "HelveticaNeue-Light","Helvetica Neue Light","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        color:white;
        font-size:12px;
        background:#333 url(/images/classy_fabric.png);
    }

    form {
        background:#111; 
        width:300px;
        margin:30px auto;
        border-radius:0.4em;
        border:1px solid #191919;
        overflow:hidden;
        position:relative;
        box-shadow: 0 5px 10px 5px rgba(0,0,0,0.2);
    }

    form:after {
        content:"";
        display:block;
        position:absolute;
        height:1px;
        width:100px;
        left:20%;
        background:linear-gradient(left, #111, #444, #b6b6b8, #444, #111);
        top:0;
    }

    form:before {
        content:"";
        display:block;
        position:absolute;
        width:8px;
        height:5px;
        border-radius:50%;
        left:34%;
        top:-7px;
        box-shadow: 0 0 6px 4px #fff;
    }

    .inset {
        padding:20px; 
        border-top:1px solid #19191a;
    }

    form h1 {
        font-size:18px;
        text-shadow:0 1px 0 black;
        text-align:center;
        padding:15px 0;
        border-bottom:1px solid rgba(0,0,0,1);
        position:relative;
    }

    form h1:after {
        content:"";
        display:block;
        width:250px;
        height:100px;
        position:absolute;
        top:0;
        left:50px;
        pointer-events:none;
        transform:rotate(70deg);
        background:linear-gradient(50deg, rgba(255,255,255,0.15), rgba(0,0,0,0));

    }

    label {
        color:#666;
        display:block;
        padding-bottom:9px;
    }

    input[type=text],
    input[type=password] {
        width:100%;
        padding:8px 5px;
        background:linear-gradient(#1f2124, #27292c);
        border:1px solid #222;
        box-shadow:
            0 1px 0 rgba(255,255,255,0.1);
        border-radius:0.3em;
        margin-bottom:20px;
    }

    label[for=remember]{
        color:white;
        display:inline-block;
        padding-bottom:0;
        padding-top:5px;
    }

    input[type=checkbox] {
        display:inline-block;
        vertical-align:top;
    }

    .p-container {
        padding:0 20px 20px 20px; 
    }

    .p-container:after {
        clear:both;
        display:table;
        content:"";
    }

    .p-container span {
        display:block;
        float:left;
        color:#0d93ff;
        padding-top:8px;
    }

    input[type=submit] {
        padding:5px 20px;
        border:1px solid rgba(0,0,0,0.4);
        text-shadow:0 -1px 0 rgba(0,0,0,0.4);
        box-shadow:
            inset 0 1px 0 rgba(255,255,255,0.3),
            inset 0 10px 10px rgba(255,255,255,0.1);
        border-radius:0.3em;
        background:#0184ff;
        color:white;
        float:right;
        font-weight:bold;
        cursor:pointer;
        font-size:13px;
    }

    input[type=submit]:hover {
        box-shadow:
            inset 0 1px 0 rgba(255,255,255,0.3),
            inset 0 -10px 10px rgba(255,255,255,0.1);
    }

    input[type=text]:hover,
    input[type=password]:hover,
    label:hover ~ input[type=text],
    label:hover ~ input[type=password] {
        background:#27292c;
    }
</style>
<div class="body">
<section id="wrapper" class="login-register">
    <div class="login-box">    
        <div class="white-box">
            <form class="form-horizontal form-material" id="loginform" action="<?= base_url(); ?>Admin/User/Login">
                <h1>Employer Log in</h1>
                <div class="inset">
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup" style="color:grey;">Remember me</label>
                            </div>
                            <a href="javascript:void(0)" id="to-recover" class="d-flex justify-content-center"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <a  href="<?= base_url(); ?>Admin/User/Login/Dashboard"class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</a>
                    </div>
                </div>

                <div class="form-group m-b-0">
                    <div class="card-footer col-sm-12">
                        <p style="color:red">Don't have an account? <a href="register.html" class="text-primary m-l-5" style="color:violet;"><b>Sign Up</b></a></p>
                    </div>
                </div>

            </form>
            <form class="form-horizontal" id="recoverform" action="">
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Email">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</section>
</div>




 <form class="form-horizontal form-material" id="loginform" action="<?= base_url() ?>Admin/User/Login">
                <h3 class="box-title m-b-20">Sign In</h3>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input type="text"  class="form-control" type="text" required="" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="text"  class="form-control" type="password" required="" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup"style="color:gray;"> Remember me </label>
                        </div>
                        <a href="javascript:void(0)" id="to-recover" class="d-flex justify-content-center"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <a href="<?= base_url() ?>Admin/User/Login/Dashboard" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</a>
                    </div>
                </div>
               
                <div class="form-group m-b-0">
                    <div class="d-flex justify-content-center links">
                        <p style = "color:gray;">Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                    </div>
                </div>
                
            </form>
            <form class="form-horizontal" id="recoverform" action="">
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Recover Password</h3>
                        <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Email">
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                    </div>
                </div>
            </form>