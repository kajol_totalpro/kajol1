<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"></h4>
            </div>

            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--<a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>-->

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="container">
            <div class ="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading" style="text-align: center; font-size: large;" >
                            <div class="pull-right"><a href="#" data-perform="panel-collapse"></a> <a href="#" data-perform="panel-dismiss"></a> </div>
                        </div>

                        <div class="panel-wrapper collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Select Plan*</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class=" ti-location-pin"></i></div>
                                                <select class="form-control">
                                                    <option>test 1</option>
                                                    <option>test 2</option>
                                                    <option>test3</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>  


                                    <div class="form-group">
                                        <label for="address" class="col-sm-3 control-label">Price* </label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <input type="radio" name="radio" value="1 month">1 month($12/month)   
                                                <div class="input-group">
                                                    <input type="radio" name="radio" value="3 month">3 month($123/month)  
                                                </div>
                                                <input type="radio" name="radio" value="8 month">8 month($1234/month)  
                                                <div class="input-group">
                                                    <input type="radio" name="radio" value="12 month">12 month($12345/month) 
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputuicence" class="col-sm-3 control-label">Licence*</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-key"></i></div>
                                                <input type="text" class="form-control" id="exampleInputlicence" placeholder="licence">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Payment*</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-money"></i></div>
                                                <select class="form-control">
                                                    <option>select payment method</option>
                                                    <option>Cheque</option>
                                                    <option>Neft</option>
                                                    <option>Paypal</option>
                                                    <option>stripe</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label for="total" class="col-sm-3 control-label">Total </label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class=" fa fa-money"></i></div>
                                                <input type="total" id="total" placeholder="total" class="form-control" name= "total">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="actualpayment" class="col-sm-3 control-label">Actual Payment </label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class=" fa fa-money"></i></div>
                                                <input type="actualpayment" id="actualpayment" placeholder="actualpayment" class="form-control" name= "actualpayment">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-6 col-md-offset-5">
                                            <div class="input-group">
                                                <a  href="<?= base_url(); ?>Admin/User/Login/showPlan" type="submit" class="btn btn-info btn-rounded" style="color: white;" >SAVE</a>

                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>







                <!--<div class="col-md-3"></div>-->






            </div>         
        </div>
    </div>
</div>