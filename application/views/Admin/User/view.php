<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title" style="color:blue;">USER</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <!--<a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>-->
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <!--                    <li><a href="#">Table</a></li>-->
                    <li class="active">Users</li>
                </ol>
            </div>
        </div>
        <div class="col-lg-12 col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading"> USER
                    <div class="pull-right"><a href="#" data-perform="panel-collapse" ><i class="ti-minus"></i></a></div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
<!--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>-->
                        <div class="col-lg-12 col-sm-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">ADVANCED SEARCH
                                    <div class="panel-action"><a href="#" data-perform="panel-collapse" ><i class="ti-minus"></i></a></div>
                                </div>
                                <div class="panel-wrapper collapse in" aria-expanded="true" style="">
                                    <div class="panel-body">
<!--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>-->
                                        <div class="form-group col-sm-4">
                                            <label for="exampleInputDate" class="col-sm-2 control-label"><b>Date:</b></label>
                                            <div class="cal-sm-2">
                                                <div class="input-group">
                                                    <input type="date" class="form-control" id="myForm">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="exampleInputEmail1" class="col-sm-3 control-label"><b>Email:</b></label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-email"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="exampleInputAddress" class="col-sm-3 control-label"><b>Form:</b></label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
<!--                                                    <div class="input-group-addon"><i class="ti-location-pin"></i></div>-->
                                                    <input type="text" class="form-control" id="exampleInputForm" placeholder="Enter Address">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="exampleInputCountry" class="col-sm-3 control-label"><b>Country:</b></label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-location-pin"></i></div>
                                                <select class="form-control">
                                                    <option> select country</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="exampleInputcity" class="col-sm-3 control-label"><b>City:</b></label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-location-pin"></i></div>
                                                <select class="form-control">
                                                    <option> select city</option>
                                                </select>
                                            </div>
                                        </div>
                                           <div class="form-group col-sm-4">
                                        <button class="fcbtn btn btn-info btn-outline btn-1f">Search</button>
                                           </div>
<!--                                        <a class="btn btn-info m-t-10">Demo button</a> </div>-->
<!--                                    <div class="panel-footer"> Panel Footer </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
        <div class="row">
            <div class="col-lg-12 col-md-4 col-sm-4 col-xs-4">
                <div class="white-box">
                    <!--                    <div class="panel panel-info">
                                        <h3 class="box-title col-md-12">User</h3>
                                            <div class="panel-heading" style="font-size:large"> USERS 
                                                <div class="pull-right"><i class="ti-minus"></i> </div>
                                                </div>
                                        </div>  -->


<!--                    <a class="btn btn-info btn-rounded " href="<?= base_url(); ?>Admin/User/Studins/Form"><i class="fa fa-plus"><b>Add New User</b></i></a>   -->

                    <div class="table-responsive">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <button class="btn btn-block btn-outline btn-rounded btn-info" style="margin-bottom: 20px;"><i class="fa fa-plus">Add New User</i></button>
                        </div>
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Fullname</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>                               



                                <tr>

                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a><i class="fa fa-pencil-square"><b>Addplan</b></i></a><span></span> 
                                        <a><i class="fa fa-edit"><b>Edit</b></i></a>

                                    </td>

                                </tr>



                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>