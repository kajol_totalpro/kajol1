<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Show Plan</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

            </div>
            <!-- /.col-lg-12 -->
        </div>


        <!-- /.row -->

        <div class="panel panel-info">
            <div class="panel-heading"> Plan
                <div class="pull-right"><a href="#" data-perform="panel-collapse"></a> <a href="#" data-perform="panel-dismiss"></a> </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">

            </div>


            <div class="row">
                <div class= "col-lg-12 col-md-4 col-sm-4 col-xs-4">

                    <div class="white-box">

                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <a href="<?= base_url(); ?>Admin/User/Login/planForm" class="btn btn-block btn-outline btn-rounded btn-info" style="margin-bottom: 20px;">+Add New Plan</a>
                        </div>
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Plan Name </th>
                                    <th>1 Month</th>
                                    <th>3 Month</th>
                                    <th>6 month</th>
                                    <th>12 Month</th>
                                    <th>Module</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a href=""><i class="fa fa-eye">View</i></a><span></span>
                                        <!--<a href=""><i class="fa fa-pencil-square">Addplan</i></a><span></span>-->
                                        <a href="<?= base_url(); ?>Admin/User/Login/planForm" ><i class="fa fa-edit">Edit</i></a>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>