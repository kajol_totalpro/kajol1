<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">USER</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">AddUser</a></li>
                    <!--            <li class="active">Notifications</li>-->
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-9">
                        <div class="panel panel-info">
                            <div class="panel-heading" style="text-align:center;font-size:large;"> USER REGISTER 
                                <div class="pull-right"> </div>
                            </div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label for="exampleInputuname" class="col-sm-3 control-label"><b>Date:</b></label>
                                            <div class="col-sm-9">

                                                <div class="input-group">
                                                    <input type="date" class="form-control" id="myForm">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputuname" class="col-sm-3 control-label"><b>FullName:</b></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-text"></i></div>
                                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="enter full name">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-3 control-label"><b>Password:</b></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                                    <input type="password" class="form-control" id="exampleInputpwd2" placeholder="Enter pwd">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="col-sm-3 control-label"><b>Email:</b></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-email"></i></div>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputMobileNumber" class="col-sm-3 control-label"><b>Mobile Number:</b></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                                    <input type="text" class="form-control" id="exampleInputMobileNumber" placeholder="Enter mobile number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputAddress" class="col-sm-3 control-label"><b>Address:</b></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-location-pin"></i></div>
                                                    <input type="text" class="form-control" id="exampleInputAddress" placeholder="Enter Address">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputCountry" class="col-sm-3 control-label"><b>Country:</b></label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-location-pin"></i></div>
                                                <select class="form-control">
                                                    <option> select country</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputcity" class="col-sm-3 control-label"><b>City:</b></label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="ti-location-pin"></i></div>
                                                <select class="form-control">
                                                    <option> select city</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label for="examplepincode" class="col-sm-3 control-label"><b>Pincode:</b></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="ti-location-pin"></i></div>
                                                    <input type="text" class="form-control" id="exampleInputPincode" placeholder="Enter Pincode">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-offset-5 col-md-3">
                                            <a href="<?= base_url(); ?>Admin/User/Login/View"class="btn btn-info btn-rounded" value="register"  style="color:white;">Register</a>
                                        </div>
<!--                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Sign in</button>-->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--        <div class="row">
                    <div class="col-sm-offset-3 col-sm-4">
        
                        <div class="panel panel-info">
                            <div class="panel-heading" style="text-align:center;"> USER REGISTER </div>
        
                        </div>
                        <div class="form-group">
                            <form id="myForm"  style="font-size:15px;" >
                                Date: <input type="date">
                            </form>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">FullName:</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="ti-text"></i></div>
                                    <input type="text" class="form-control" id="exampleInputuname" placeholder="enter full name">
                                </div>
                            </div>
                        </div>
        
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password:</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                    <input type="password" class="form-control" id="exampleInputpwd2" placeholder="Enter pwd">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="col-sm-3 control-label">Email:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputMobileNumber" class="col-sm-3 control-label">Mobile Number:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-mobile"></i></div>
                                        <input type="text" class="form-control" id="exampleInputMobileNumber" placeholder="Enter mobile number">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputAddress" class="col-sm-3 control-label">Address:</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-microphone-alt"></i></div>
                                        <input type="text" class="form-control" id="exampleInputAddress" placeholder="Enter Address">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>-->
    </div>
</div>
<!--


















