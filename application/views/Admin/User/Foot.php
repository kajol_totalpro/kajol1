<html>

<!-- Bootstrap Core JavaScript -->
<!--<script src="<?= base_url()?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<script src="<?= base_url()?>assets/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Menu Plugin JavaScript -->
<script src="<?= base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?= base_url()?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?= base_url()?>assets/js/waves.js"></script>
<script src="<?= base_url() ?>assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?= base_url() ?>assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?= base_url() ?>assets/plugins/bower_components/raphael/raphael-min.js"></script>
<!--<script src="<?= base_url() ?>assets/plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?= base_url() ?>assets/js/custom.min.js"></script>
<!--<script src="<?= base_url() ?>assets/js/dashboard1.js"></script>-->

<script src="<?= base_url() ?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>


<!-- start - This is for export functionality only -->
<script src="<?= base_url() ?>assets/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="<?= base_url() ?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<!-- Sparkline chart JavaScript -->
<script src="<?= base_url() ?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?= base_url() ?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script type="text/javascript">

   $(document).ready(function(){
      $('#myTable').DataTable();
      $(document).ready(function() {
        var table = $('#example').DataTable({
          "columnDefs": [
          { "visible": false, "targets": 2 }
          ],
          "order": [[ 2, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;

            api.column(2, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                  );

                last = group;
              }
            } );
          }
        });
        });
        });
</script>
<script src="<?= base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!--<script type="text/javascript">
    / <![CDATA[ 
            var google_conversion_id = 997656527;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    / ]]> /
</script>-->

</body>


</<html>
    


<!-- Mirrored from eliteadmin.themedesigner.in/demos/eliteadmin-inverse/form-basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 18 Sep 2016 12:04:25 GMT -->
