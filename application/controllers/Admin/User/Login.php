<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
//        $this->load->model('Insert_m', 'im');
    }

    public function index() {
        $this->load->view('Admin/User/head');
//        $this->load->view('Admin/Header');
        $this->load->view('Admin/User/login');
        $this->load->view('Admin/User/Foot');
//
//        $this->load->view('Admin/Footer');
    }

    public function Dashboard() {
        $this->load->view('Admin/User/head');
        $this->load->view('Admin/Header');
        $this->load->view('Admin/User/form');
        $this->load->view('Admin/User/Foot');
//
        $this->load->view('Admin/Footer');
    }
     public function View() {
        $this->load->view('Admin/User/head');
        $this->load->view('Admin/Header');
        $this->load->view('Admin/User/view');
        $this->load->view('Admin/User/Foot');
//
        $this->load->view('Admin/Footer');
    }
    public function planForm() {
        $this->load->view('Admin/User/head');
        $this->load->view('Admin/Header');
        $this->load->view('Admin/User/planform');
        $this->load->view('Admin/User/Foot');
//
        $this->load->view('Admin/Footer');
    }

}
